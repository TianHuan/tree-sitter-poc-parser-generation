from ast import node_name, TYPE_INHERIT, SUPER_TYPES, ALL_TYPES



def handle_item(item):
    codes = ""
    if item['named'] is False:
        return ""
    if 'subtypes' in item:
        return ""

    codes += f"    # callback: {item['type']}\n"
    codes += f"    def leave_{item['type']}(self, _):\n"
    codes += f"        print('leave: ', '{item['type']}')\n"
    codes += "        return _\n"
    codes += "\n"
    return codes

def callback(node_types):
    codes = ""
    codes += """
class TSCallback:

    def __init__(self):
        self.contents = b''
        
    def set_content(self, contents):
        self.contents = contents
        
    def contents(self, node):
        return self.contents[node.pos.start_range:node.pos.end_range]
    
"""
    for item in node_types:
        codes += handle_item(item)

    return codes