from ast import node_name, TYPE_INHERIT, SUPER_TYPES, ALL_TYPES

ENTRY = "program"

def _hedaer():
    return """
from merico.graphserver.data_structure import NodePos
from merico.parser.tree_sitter.tree_sitter import build_parser
from merico.parser.typescript_poc.callback import TSCallback
from merico.parser.typescript_poc.ast import *


class TSParser:

    def __init__(self):
        self.content = ''
        self.callback = TSCallback()
        language_name = 'typescript'
        self.parser = build_parser(language_name)

    def parse(self, content):
        self.content = content
        parse_tree = self.parser.parse(content)
        return self.transform(parse_tree)

    def handle_common(self, node, cursor):
        if cursor.node.has_error:
            node.errors.extend(self.transform_error(cursor, node))
            return True

        if cursor.node.type == "comment":
            comment_node = tsast_comment()
            comment_node.errors = []
            comment_node.comments = []
            comment_node.children = []
            start_line, start_col = cursor.node.start_point
            end_line, end_col = cursor.node.end_point
            comment_node.pos = NodePos(start_line + 1, start_col + 1, end_line + 1, end_col + 1, cursor.node.start_byte, cursor.node.end_byte)
            node.comments.append(comment_node)
            return True
            
        if not cursor.node.is_named:
            return True

        return False

    def transform(self, tree):
        cursor = tree.walk()
        return self.transform_""" + ENTRY + """(cursor, None)

"""

def _footer():
    return """

parse = TSParser().parse
"""

def _transform_error():
    codes = ""
    codes += "    def transform_error(self, cursor, parent):\n"
    codes += "        nodes = []\n"
    codes += "        if cursor.goto_first_child():\n"
    codes += "            while True:\n"
    codes += "                kind = cursor.node.type\n"
    codes += "                if False: \n"
    codes += "                    pass\n"
    for t in ALL_TYPES:
        codes += f"                elif kind == '{t}':\n"
        codes += f"                    node = self.transform_{t}(cursor, parent)\n"
        codes += "                    if node is not None:\n"
        codes += "                        nodes.append(node)\n"

    codes += "                if not cursor.goto_next_sibling():\n"
    codes += "                    break\n"

    codes += "            cursor.goto_parent()\n"
    codes += "            return nodes\n"
    codes += "        else:\n"
    codes += "            return nodes\n"
    codes += "\n"

    return codes


def has_multi_types(types):
    named_types = []
    for t in types:
        if t['named']:
            named_types.append(t)
    return len(named_types)


def _func_name(name):
    return f"    def transform_{name}(self, cursor, parent):\n"


def handle_supertype(node_type):
    if node_type['named'] is False:
        return ""

    codes = ""
    codes += _func_name(node_type['type'])
    codes += f"        node = None\n"
    codes += f"        kind = cursor.node.type\n"

    types_handled = set()
    for subtype in node_type.get('subtypes', []):
        if subtype['named'] is False:
            continue

        real_types = gen_case(subtype['type'])
        st = ''
        for records in real_types:
            filted_records = []
            rkeys = [x for x in records.keys()]
            rkeys.sort()
            for r in rkeys:
                if r not in types_handled:
                    types_handled.add(r)
                    filted_records.append(r)
                    st = records[r]
            if len(filted_records) == 0:
                continue
            filted_records.sort()
            case_conditions = '", "'.join(filted_records)

            codes += f'        if kind in ["{case_conditions}"]:\n'
            codes += f"            node = self.transform_{st}(cursor, parent)\n"

    
    codes += "        return node\n"
    codes += "\n"
    codes += "\n"
    return codes


def gen_case(item_type):
    result = []
    if item_type in SUPER_TYPES:
        record = {}
        for st in SUPER_TYPES[item_type]:
            if st in SUPER_TYPES:
                st_result = gen_case(st)
                result.extend(st_result)
            else:
                record[st] = item_type
        result.append(record)
    else:
        record = {}
        record[item_type] = item_type
        result.append(record)
    return result


def handle_item(node_type):
    if node_type['named'] is False:
        return ""

    codes = ""
    codes += _func_name(node_type['type'])
    #codes += f"        self.enter_{node_type['type']}()\n"
    codes += f"        node = {node_name(node_type['type'])}()\n"
    codes += "        node.parent = parent\n"
    codes += "        node.errors = []\n"
    codes += "        node.comments = []\n"
    codes += "        node.children = []\n"
    codes += "        start_line, start_col = cursor.node.start_point\n"
    codes += "        end_line, end_col = cursor.node.end_point\n"
    codes += "        node.pos = NodePos(start_line + 1, start_col + 1, end_line + 1, end_col + 1, cursor.node.start_byte, cursor.node.end_byte)\n"
    codes += "\n"
    codes += "        if cursor.goto_first_child():\n"
    codes += "            while True:\n"
    codes += "                if self.handle_common(node, cursor):\n"
    codes += "                    if not cursor.goto_next_sibling():\n"
    codes += "                        break\n"
    codes += "                    continue\n"
    codes += "\n"

    if node_type.get('fields', None) or node_type.get('children', None):
        codes += "                field_name = cursor.current_field_name()\n"
        codes += "                if False:\n"
        codes += "                    pass\n"
    
        fields_keys = [x for x in node_type.get('fields', {}).keys()]
        fields_keys.sort()
        for fieldName in fields_keys:
            fieldItem = node_type.get('fields', {})[fieldName]
            codes += f"                elif field_name == \"{fieldName}\":\n"
            codes += "                    kind = cursor.node.type\n"
            codes += "                    if False:\n"
            codes += "                        pass\n"
            if fieldItem['multiple']:
                types_handled = set()
                for item in fieldItem['types']:
                    if item['named']:
                        real_types = gen_case(item['type'])
                        st = ''
                        for records in real_types:
                            filted_records = []
                            rkeys = [x for x in records.keys()]
                            rkeys.sort()
                            for r in rkeys:
                                if r not in types_handled:
                                    types_handled.add(r)
                                    filted_records.append(r)
                                    st = records[r]
                            if len(filted_records) == 0:
                                continue
                            filted_records.sort()
                            case_conditions = '", "'.join(filted_records)
                            codes += f'                    elif kind in ["{case_conditions}"]:\n'
                            codes += f"                        child = self.transform_{st}(cursor, parent)\n"
                            codes += f"                        if child is not None:\n"
                            codes += f"                            node.{fieldName}.append(child)\n"

            else:
                types_handled = set()
                for item in fieldItem['types']:
                    if item['named']:
                        real_types = gen_case(item['type'])
                        st = ''
                        for records in real_types:
                            filted_records = []
                            rkeys = [x for x in records.keys()]
                            rkeys.sort()
                            for r in rkeys:
                                if r not in types_handled:
                                    types_handled.add(r)
                                    filted_records.append(r)
                                    st = records[r]
                            if len(filted_records) == 0:
                                continue
                            filted_records.sort()
                            case_conditions = '", "'.join(filted_records)
                            codes += f'                    elif kind in ["{case_conditions}"]:\n'
                            codes += f"                        node.{fieldName} = self.transform_{st}(cursor, parent)\n"

        codes += "                else:\n"
        codes += "                    kind = cursor.node.type\n"
        codes += "                    if False:\n"
        codes += "                        pass\n"

        types_handled = set()
        for item in node_type.get('children', {}).get('types', []):
            if not item['named']:
                continue

            real_types = gen_case(item['type'])
            st = ''
            for records in real_types:
                filted_records = []
                rkeys = [x for x in records.keys()]
                rkeys.sort()
                for r in rkeys:
                    if r not in types_handled:
                        types_handled.add(r)
                        filted_records.append(r)
                        st = records[r]
                if len(filted_records) == 0:
                    continue
                filted_records.sort()
                case_conditions = '", "'.join(filted_records)
                codes += f'                    elif kind in ["{case_conditions}"]:\n'
                codes += f"                        child = self.transform_{st}(cursor, node)\n"
                codes += "                        if child is not None:\n"
                if node_type['children']['multiple']:
                    codes += f"                            node.children.append(child)\n"
                else:
                    codes += f"                            node.children = child\n"

                
    codes += "                if not cursor.goto_next_sibling():\n"
    codes += "                    break\n"

    codes += "            cursor.goto_parent()\n"
    codes += f"        return self.callback.leave_{node_type['type']}(node)\n"
    codes += "\n"
    return codes


def transform(entry, node_types):
    global ENTRY
    ENTRY = entry
    codes = ""
    codes += _hedaer()
    codes += _transform_error()

    for item in node_types:
        if "subtypes" in item:
            codes += handle_supertype(item)
        else:
            codes += handle_item(item)

    codes += _footer()
    return codes
