BASE_AST = 'AST'
TYPE_INHERIT = {}
SUPER_TYPES = {}
ALL_TYPES = []

KEYWORD_MAPPING = {
    'operator': 'op',
}

def node_name(name):
    return f"{BASE_AST.lower()}_{name}"

def _header():
    return """
class """ +  BASE_AST + """:
        pass

"""

def _extra():
    codes = ''
    kinds = ['comment']
    for kind in kinds:
        codes += f"""
class {node_name(kind)}(tsast):
    class_name = "{node_name(kind)}"

"""
    return codes


def _footer():
    return ""

def gen_type_inherit(node_types):
    global TYPE_INHERIT
    global SUPER_TYPES
    global ALL_TYPES
    for item in node_types:
        if 'subtypes' not in item:
            if item['named']:
                ALL_TYPES.append(item['type'])
            continue
        if not item['named']:
            continue

        super_type = item['type']
        if super_type not in SUPER_TYPES:
            SUPER_TYPES[super_type] = []

        for sub_type in item['subtypes']:
            if sub_type['named']:
                TYPE_INHERIT[sub_type['type']] = super_type
                SUPER_TYPES[super_type].append(sub_type['type'])

    return ""

def get_type_class(v):
    named_items = []
    comments = "# subtypes: "
    for item in v.get('types', []):
        if item['named']:
            named_items.append(item)
    if len(named_items) == 1:
        return node_name(named_items[0]['type']), ""
    elif len(named_items) == 0:
        return "", ""
    else:
        for item in named_items:
            comments += " "
            comments += item['type']
        return BASE_AST, comments


def get_field_name(name):
    if name in KEYWORD_MAPPING:
        return KEYWORD_MAPPING[name]
    else:
        return name


def gen_ast(node_types):
    codes = ""
    for item in node_types:
        if item['named'] is False:
            continue

        type_name = item['type']
        if type_name in SUPER_TYPES:
            base_ast = BASE_AST
            tn = type_name
            while tn in TYPE_INHERIT:
                tn = TYPE_INHERIT[tn]
                base_ast = tn
                break
            if base_ast != BASE_AST:
                base_ast = node_name(base_ast)

            codes += f"class {node_name(type_name)}({base_ast}):\n"
            codes += f'    class_name = "{node_name(type_name)}"\n\n'
            for subtype in SUPER_TYPES[type_name]:
                codes += f"    # subtype: {node_name(subtype)}\n"
        else:
            base_ast = BASE_AST
            if type_name in TYPE_INHERIT:
                base_ast = node_name(TYPE_INHERIT[type_name])
            codes += f"class {node_name(type_name)}({base_ast}):\n"
            codes += f'    class_name = "{node_name(type_name)}"\n\n'
            fields_keys = [x for x in item.get('fields', {}).keys()]
            fields_keys.sort()
            for k in fields_keys:
                v = item['fields'][k]
                type_class, comments = get_type_class(v)
                if type_class == "":
                    continue
                if comments != "":
                    codes += f"    {comments}\n"

                if v.get('multiple', False):
                    type_class = '[' + type_class + ']'
                    codes += f"    # type: {type_class}\n"
                    codes += f"    {get_field_name(k)} = []\n"
                else:
                    codes += f"    # type: {type_class}\n"
                    codes += f"    {get_field_name(k)} = None\n"

            if item.get('children', None) and item['children']:
                v = item['children']
                type_class, comments = get_type_class(v)
                if type_class == "":
                    continue
                if comments != "":
                    codes += f"    {comments}\n"
                    
                if v.get('multiple', False):
                    type_class = '[' + type_class + ']'
                    codes += f"    # type: {type_class}\n"
                    codes += f"    children = []\n"
                else:
                    codes += f"    # type: {type_class}\n"
                    codes += f"    children = None\n"


        codes += "\n"
    
    return codes


def ast(base_ast, node_types):
    global BASE_AST
    BASE_AST = base_ast

    gen_type_inherit(node_types)

    codes = ""
    codes += _header()
    codes += _extra()
    codes += gen_ast(node_types)
    codes += _footer()
    return codes