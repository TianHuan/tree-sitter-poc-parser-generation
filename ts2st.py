import json
import sys
import os
import datetime
from shutil import copyfile

from ast import ast
from transform import transform
from callback import callback


def run(entry, base_ast, src, dst):
    with open(src, 'r') as f:
        node_types = json.load(f)

    # sort
    node_types.sort(key=lambda x: x['type'])
    for item in node_types:
        if 'subtypes' in item:
            item['subtypes'].sort(key=lambda x: x['type'])
        if 'children' in item:
            item['children']['types'].sort(key=lambda x: x['type'])
        if 'fields' in item:
            for k, v in item['fields'].items():
                v['types'].sort(key=lambda x: x['type'])
    
    ast_codes = ast(base_ast, node_types)
    transform_codes = transform(entry, node_types)
    callback_codes = callback(node_types)

    # save files
    # create dir
    dir_name = f'ts2st_{base_ast}_{datetime.datetime.now().strftime("%Y%m%d%H%M%S")}'
    dir_path = os.path.join(dst, dir_name)
    print(f"output dir: {dir_path}")
    os.mkdir(dir_path)

    # copy node-types.json
    copyfile(src, os.path.join(dir_path, 'node-types.json'))

    # gen ast
    with open(os.path.join(dir_path, 'ast.py'), 'w') as f:
        #f.write(f'# This file is auto-generated from {dir_name}\n')
        f.write(ast_codes)

    # gen transform
    with open(os.path.join(dir_path, 'transform.py'), 'w') as f:
        #f.write(f'# This file is auto-generated from {dir_name}\n')
        f.write(transform_codes)

    # gen callback
    with open(os.path.join(dir_path, 'callback.py'), 'w') as f:
        #f.write(f'# This file is auto-generated from {dir_name}\n')
        f.write(callback_codes)


def usage():
    return "Usage: ts2st [entry] [base ast] [src/node-types.json] [dst]"


if __name__ == '__main__':
    args = sys.argv
    if len(args) != 5:
        print(usage())
        sys.exit(1)
    run(args[1], args[2], args[3], args[4])
