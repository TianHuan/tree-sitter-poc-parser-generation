# USAGE

```
python ts2st.py [grammar.js entry rule name] [ast base name] [node-types.json path] [output directory]

(ma) ➜  ts2st git:(master) python ts2st.py program tsast ~/merico/meta-analytics/merico/parser/tree_sitter/parsers/tree-sitter-typescript/typescript/src/node-types.json /tmp
output dir: /tmp/ts2st_tsast_20201214120457
(ma) ➜  ts2st git:(master) ll /tmp/ts2st_tsast_20201214120457
total 1200
-rw-r--r--  1 qintianhuan  wheel    38K 12 14 12:04 ast.py
-rw-r--r--  1 qintianhuan  wheel    21K 12 14 12:04 callback.py
-rw-r--r--  1 qintianhuan  wheel   116K 12 14 12:04 node-types.json
-rw-r--r--  1 qintianhuan  wheel   417K 12 14 12:04 transform.py
```
